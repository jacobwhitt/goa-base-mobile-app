import React, { useState, useEffect, useCallback } from 'react';
import moment from 'moment';
import { StyleSheet, Text, View, TextInput, Button, ImageBackground, ScrollView, AsyncStorage, AppRegistry, FlatList, Image, Picker, TouchableOpacity, SafeAreaView, } from 'react-native';
import { Linking } from 'expo';
import { Ionicons, Foundation, FontAwesome, FontAwesome5, MaterialIcons, MaterialCommunityIcons, AntDesign, Entypo, Octicons, Zocial } from '@expo/vector-icons';

export default function Event(props) {
  console.log(`You're in the event container`)
  console.log('props in Event.js', props)

  const [favorite, setFavorite] = useState(false)
  const eventURL = props.event.urlOrganizer


  useEffect(() => {
    const _getFavorites = async () => {
      try {
        const favorites = await AsyncStorage.getItem('favorites');
        console.log('favIDs in Async IN EVENT', favorites)
        let temp = JSON.parse(favorites)
        let idx = temp.findIndex(ele => ele === props.event.id)
        if (idx !== -1) {
          setFavorite(!favorite)
        }
      }
      catch (error) {
        console.log(error)
      }
    };
    _getFavorites()
  }, [])


  const _saveFavorite = async () => {
    try {
      const favorites = await AsyncStorage.getItem('favorites');
      console.log("temp in saveFav in EVENT", favorites)
      let temp = JSON.parse(favorites)

      let idx = temp.findIndex(ele => ele === props.event.id)
      idx === -1 ? temp.push(props.event.id) : temp.splice(idx, 1)

      await AsyncStorage.setItem('favorites', JSON.stringify([...temp]))
      setFavorite(!favorite);
      
      props.resetState();
      const favorites2 = await AsyncStorage.getItem('favorites');
      console.log('asyncStorage after saving in EVENT', favorites2)
      
      // if (props.index === 1) {
      //   console.log('IN EVENT > ASYNC SAVE FUNC')
      //   props.resetState();
      // }

    }
    catch (e) {
      console.log(e)
    }
  }

  return (
    <>

      <View style={styles.top}>
        <Text style={props.event.nameParty.length > 24 ? styles.eventNameSmall : styles.eventNameBig}>{props.event.nameParty}</Text>
      </View>

      <View style={styles.bottom}>

        {
          props.event.urlImageSmall !== null && props.event.urlImageFull !== null ? <Image source={{ uri: props.event.urlImageFull }} style={styles.imageContainer} />
            : props.event.urlImageMedium !== null && props.event.urlImageFull !== null ? <Image source={{ uri: props.event.urlImageFull }} style={styles.imageContainer} />
              : props.event.urlImageFull !== null ? <Image source={{ uri: props.event.urlImageFull }} style={styles.imageContainer} />
                : <Image source={{ uri: 'https://static.umotive.com/img/product_image_thumbnail_placeholder.png' }} style={styles.imageContainer} />
        }

        <View style={styles.status}>
          <Text style={styles.nameStatus}>{props.event.nameStatus}</Text>
        </View>

        <View style={styles.favorite}>
          <FontAwesome name="star" size={32} color={favorite === false ? 'white' : 'yellow'}
            onPress={() => { _saveFavorite(); }} />
        </View>


        <View style={styles.eventInfo}>
          <Text style={styles.text}>Organizer: {props.event.nameOrganizer}</Text>
          <Text style={styles.text}>Starts: {moment(props.event.dateStart).format('llll')}</Text>
          <Text style={styles.text}>Ends: {moment(props.event.dateEnd).format('llll')}</Text>
          <Text style={styles.text}>Type: {props.event.nameType}</Text>

          <View style={styles.eventLocation}>
            <Text style={styles.text}>Location: {props.event.nameTown}, {props.event.nameCountry}</Text>
          </View>


          {props.event.urlOrganizer === null ?
            null
            :
            <Text style={styles.link} onPress={() => { console.log(eventURL), Linking.openURL(eventURL) }}>External Event Page</Text>
          }

          <TouchableOpacity style={styles.locationButtons} onPress={props.displayEvent}>
            <FontAwesome name="reply" size={32} color='white' />
            <Text style={styles.text}>   Back to Events</Text>
          </TouchableOpacity>

        </View>
      </View>
    </>
  );
}


const styles = StyleSheet.create({
  container: {
    width: '100%',
    // height: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: '#150f2e',
  },
  favorite: {
    paddingLeft: 300,
  },


  top: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#562e7a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventNameSmall: {
    margin: 10,
    fontSize: 20,
    color: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventNameBig: {
    margin: 10,
    fontSize: 36,
    color: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },


  bottom: {
    width: '100%',
    height: '90%',
    margin: 0,
    backgroundColor: '#150f2e',
  },
  imageContainer: {
    width: '100%',
    height: '50%'
    // marginTop: 5,
    // marginLeft: '2.5%',
    // marginRight: '2.5%',
  },
  status: {
    marginTop: 10,
  },
  nameStatus: {
    fontSize: 24,
    color: 'red',
    textAlign: 'center',
  },
  eventInfo: {
    height: '50%',
    paddingTop: 5,
    paddingLeft: 15,
  },
  text: {
    fontSize: 20,
    color: 'white',
  },
  link: {
    color: 'lime',
    fontSize: 24,
  },
  locationButtons: {
    // flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});