# Goabase Mobile Companion

This is a companion tool for users of <a href="http://goabase.net">Goabase</a> with which they can search for events by country/city and type of events and save them for quick-viewing later. [This project requires the Expo mobile app and some basic npm installation for now until it's published for Android on Google Play]

## Getting Started

Git clone the repo here and download the Expo mobile app to open and use the app.

### Pre-requisites

```
"expo": "^37.0.0",
"expo-web-browser": "^8.1.0",
"faker": "latest",
"lodash": "latest",
"moment": "latest",
"react": "16.9.0",
"react-native": "https://github.com/expo/react-native/archive/sdk-37.0.0.tar.gz",
"react-native-elements": "latest",
"react-native-pagination": "latest",
"react-native-paper": "latest",
"react-native-really-awesome-button": "latest",
"react-native-vector-icons": "^6.6.0"
```

### Installing

After git cloning the repo and running npm install for the pre-reqs, you should be able to 
open the terminal in the repo folder 'expo start' to launch the Expo dev tools in your browser

```
npm install
expo start
```

## Basic Functions
* Search for events
* Save events in favorites tab
* Access external links to facebook events or event's host's websites

* In development: deployment as an Android app via Google Play (some of the modules are incompatible with Snack)


## Built With

* [React Native](https://reactnative.dev/) - Client framework
* [Goabase API](https://www.goabase.net/goabase_api_party.php) - Event DB JSON API
* [Expo](https://docs.expo.io/) - Web framework, build and deployment tool for iOS/Android 

## Author

* **Jacob Whitt** - [Gitlab](https://gitlab.com/jacobwhitt) [Linkedin](https://www.linkedin.com/in/jacob-tw-82305714b/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
