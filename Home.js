import React, { useState, useEffect, useCallback } from 'react';
import moment from 'moment';
import { StyleSheet, Alert, Text, View, TextInput, ImageBackground, ScrollView, AsyncStorage, AppRegistry, FlatList, Image, Picker, TouchableOpacity, SafeAreaView, } from 'react-native';
import Event from './Event'
import { Ionicons, Foundation, FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons';


export default function Home(props) {
  useEffect(() => {
    console.log('props in Home', props)
    initialDisplay(props.events)
  }, [props.index]);

  const [showEvent, setShowEvent] = useState(false);
  const [eventInfo, setEventInfo] = useState({});
  const [filtered, setFiltered] = useState([]);
  const [search, setSearch] = useState('');
  const [location, setLocation] = useState('city');
  const [eventType, setEventType] = useState('Indoor');


  const displayEvent = (itemInfo) => {
    setShowEvent(!showEvent)
    setEventInfo(itemInfo)
    // console.log('displayEvent() =>', itemInfo)
  }

  const truncate = (input) => input.length > 40 ? `${input.substring(0, 40)}...` : input;


  const Item = (args) => {
    // console.log('in item args', args)
    let nameParty = truncate(args.event.nameParty)

    return <TouchableOpacity onPress={() => displayEvent(args.event)}>

      <View style={styles.itemContainer}>

        {args.event.urlImageFull !== null ?
          <View style={styles.itemContainerLeft}>
            <Image source={{ uri: args.event.urlImageFull }} style={styles.imageContainer} />
          </View>
          : <View style={styles.itemContainerLeft}>
            <Image source={{ uri: 'https://static.umotive.com/img/product_image_thumbnail_placeholder.png' }} style={styles.imageContainer} />
          </View>
        }

        <View style={styles.itemContainerRight}>

          <Text style={{ color: 'lime', fontSize: 24 }}>{nameParty}</Text>

          <View style={styles.eventDate}>
            <Text style={{ color: 'yellow', fontSize: 18 }}>{moment(args.event.dateStart).format('ll')}</Text>
            <Text style={{ color: 'yellow', fontSize: 18 }}> • </Text>
            <Text style={{ color: 'yellow', fontSize: 18 }}>{args.event.nameType}</Text>
          </View>

          <View style={styles.eventLocation}>
            <Text style={{ color: 'lime', fontSize: 18 }}>{args.event.nameCountry}</Text>
            <Text> </Text>
            <Text style={{ color: 'lime', fontSize: 18 }}>{args.event.nameTown}</Text>
          </View>

        </View>
      </View>
    </TouchableOpacity>
  }


  const initialDisplay = (props) => {
    const limit = props.length < 100 ? props.length : 100
    const temp = []

    for (let i = 0; i < limit; i++) {
      temp.push(props[i])
    }
    temp.forEach((item, idx) => {
      item.urlOrganizer = fixURL(item.urlOrganizer)
    })
    setFiltered([...temp]);
  }


  const fixURL = (url) => {
    if (url === null) {
      // console.log('null')
      return null
    }
    else if (url.charAt(0) !== 'h' && url.charAt(0) !== 'w') {
      // console.log('http://' + url.split('\n')[0].toString())
      return 'http://' + url.split(/\r?\n/)[0].toString()
    }
    else if (url.charAt(0) === 'w') {
      // console.log('http://' + url.split('\n')[0].toString())
      return 'http://' + url.split(/\r?\n/)[0].toString()
    }
    else if (url.charAt(0) === 'h') {
      return url.split(/\r?\n/)[0].toString().trim()
    }
    console.log(url)
  }

  const filterEvents = () => {
    const temp2 = []
    console.log('in filterEvents', search.text, eventType)

    if ((location === 'city' || location === 'country') && search.text.trim() === 'undefined') {
      initialDisplay(props.events)
    }

    props.events.forEach((item, idx) => {
      if (location === 'country' && item.nameCountry === search.text.trim() && item.nameType === eventType) {
        temp2.push(item)
      }
      else if (location === 'city' && item.nameTown === search.text.trim() && item.nameType === eventType) {
        temp2.push(item)
      }
    })
    console.log('temp 2 filterEvents in HOME', temp2)
    initialDisplay(temp2);
  }


  return (
    <View style={styles.container}>
      {showEvent ?
        <Event showEvent={showEvent} displayEvent={displayEvent} event={eventInfo} index={props.index} resetState={props.resetState} />
        :
        <>
          <View style={styles.header}>

            <Image source={{ uri: 'https://www.goabase.net/images/logos/xm9kbe7hnj_psylogoab.jpg' }} style={styles.headerImage} />

            <View style={styles.locationType}>
              <TouchableOpacity style={styles.typeToggles} onPress={() => setLocation('country')}>
                <FontAwesome5 name="globe-americas" size={32} color={location === 'country' ? 'lime' : 'white'} />

                <Text style={styles.text}>Country</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setLocation('city')}>
                <Ionicons name="md-business" size={32} color={location === 'city' ? 'lime' : 'white'} />

                <Text style={styles.text}>City</Text>
              </TouchableOpacity>

              <TextInput
                style={styles.locationInput}
                placeholder={`Search by ${location}...`}
                placeholderTextColor={'white'}
                onChangeText={(text) => setSearch({ text })} />

              <TouchableOpacity style={styles.typeToggles} onPress={() => filterEvents()}>
                <Ionicons name="ios-search" size={32} color='white' />

                <Text style={styles.text}>Search</Text>
              </TouchableOpacity>
            </View>


            <View style={styles.locationType}>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setEventType('Indoor')}>
                <FontAwesome5 name="warehouse" size={32} color={eventType === 'Indoor' ? 'lime' : 'white'} />

                <Text style={styles.text}>Indoor</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setEventType('Club')}>
                <FontAwesome5 name="cocktail" size={32} color={eventType === 'Club' ? 'lime' : 'white'} />

                <Text style={styles.text}>Club</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setEventType('In- & Outdoor')}>
                <Foundation name="mountains" size={32} color={eventType === 'In- & Outdoor' ? 'lime' : 'white'} />

                <Text style={styles.text}>In/Outdoor</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setEventType('Festival')}>
                <Ionicons name="md-bonfire" size={32} color={eventType === 'Festival' ? 'lime' : 'white'} />

                <Text style={styles.text}>Festival</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.typeToggles} onPress={() => setEventType('Open Air')}>
                <Ionicons name="md-bonfire" size={32} color={eventType === 'Open Air' ? 'lime' : 'white'} />

                <Text style={styles.text}>Open Air</Text>
              </TouchableOpacity>


            </View>

          </View>

          <FlatList
            data={filtered}
            style={styles.flatlist}
            renderItem={({ item, index }) => <Item event={item} index={index} />}
            keyExtractor={item => item.id}
          />
        </>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#562e7a',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },


  header: {
    width: '95%',
    height: '40%',
    marginTop: 5,
    marginRight: '5%',
    marginLeft: '5%',
    backgroundColor: '#562e7a',
    borderBottomColor: 'yellow',
    borderBottomWidth: 1,
  },
  headerImage: {
    width: '96%',
    height: '40%',
    marginLeft: '2%',
    borderRadius: 15,
  },

  locationType: {
    marginTop: 10,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  locationInput: {
    width: '40%',
    height: 50,
    color: 'white',
    paddingLeft: 10,
    borderColor: 'lime',
    borderWidth: 1,
    borderRadius: 5,
  },


  typeToggles: {
    height: 60,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },




  flatlist: {
    width: "100%",
    marginTop: 5,
  },
  itemContainer: {
    flexDirection: 'row',
    width: '95%',
    height: 130,
    marginTop: 5,
    marginLeft: '2.5%',
  },
  imageContainer: {
    width: 120,
    height: 100,
    margin: 10,
  },
  itemContainerLeft: {
    flex: 1,
    margin: 5,
  },
  itemContainerRight: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: -60,
    marginTop: 20,
  },
  eventDate: {
    flexDirection: 'row',
  },
  eventLocation: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});

