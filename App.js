import * as React from "react";
import { Dimensions, StyleSheet, SafeAreaView, Platform } from "react-native";
import { BottomNavigation, Text } from "react-native-paper";
import AsyncStorage from "@react-native-async-storage/async-storage";

import Constants from "expo-constants";
import Home from "./Home";
import Favorites from "./Favorites";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default class GoaBase extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: "home", title: "Home", icon: "hinduism" },
      { key: "favorites", title: "Favorites", icon: "star" },
    ],
    events: [],
    favIDs: [],
  };

  componentDidMount() {
    console.log("COMPONENT DID MOUNT");
    this.initialFetch();
    this.readStorage();
  }

  initialFetch = async () => {
    console.log("INIT FETCH");
    fetch("https://www.goabase.net/api/party/json/")
      .then((res) => res.json())
      .then((result) => {
        console.log("initial fetch", result);
        this.setState({ events: [...result.partylist] });
      })
      .catch((error) => console.log(error));
  };

  readStorage = async () => {
    console.log("READ STORAGE");
    try {
      const favorites = await AsyncStorage.getItem("favorites");
      console.log("Async in APP", favorites);
      if (favorites === null) {
        await AsyncStorage.setItem("favorites", JSON.stringify([]));
      } else {
        let temp = JSON.parse(favorites);
        this.setState({ favIDs: [...temp] }, () => this.initialFetch());
      }
    } catch (error) {
      console.log(error);
    }
  };

  resetState = async () => {
    console.log("resetState in APP", this.state);
    const favorites = await AsyncStorage.getItem("favorites");
    let temp = JSON.parse(favorites);
    this.setState({ favIDs: [...temp] });
  };

  _handleIndexChange = (index) => {
    this.setState({ index }), console.log("handleIndexChange in APP", index);
  };

  renderScene = ({ route }) => {
    switch (route.key) {
      case "home":
        return <Home favIDs={this.state.favIDs} events={this.state.events} index={this.state.index} resetState={this.resetState} />;
      case "favorites":
        return <Favorites favIDs={this.state.favIDs} events={this.state.events} index={this.state.index} resetState={this.resetState} />;
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.app_container}>
        <BottomNavigation style={styles.btmnav} shifting={true} navigationState={this.state} onIndexChange={this._handleIndexChange} renderScene={this.renderScene} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  btmnav: {
    justifyContent: "space-between",
  },
  app_container: {
    backgroundColor: "#150f2e",
    width: windowWidth,
    height: windowHeight,
    marginTop: Platform.OS === "ios" ? null : Constants.statusBarHeight,
  },
});
