import React, { useState, useEffect, useCallback } from 'react';
import moment from 'moment';
import { StyleSheet, Text, View, TextInput, ImageBackground, AsyncStorage, AppRegistry, FlatList, Image, TouchableOpacity, SafeAreaView, } from 'react-native';
import { Ionicons, Foundation, FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons';
import Event from './Event'



export default function Favorites(props) {
  
  const [showEvent, setShowEvent] = useState(false);
  const [eventInfo, setEventInfo] = useState({});
  const [events, setEvents] = useState([]);
  const [filtered, setFiltered] = useState([]);
  

  useEffect(() => {
    if (props.events.length !== 0) {
      displayFavs();
    }
  }, [props.favIDs])


  const displayFavs = () => {
    console.log('displayFavs in FAVORITES')
    let temp = []
    props.favIDs.forEach((id) => {
      props.events.forEach((item) => {
        if (id === item.id) {
          // console.log('temp1', temp)
          temp.push(item)
        }
      })
    })
    console.log('temp2 in displayFavs', temp)
    setFiltered(temp);
  }

  // console.log('ITEMS FILTERED IN FAVS', filtered)

  const displayEvent = (itemInfo) => {
    setShowEvent(!showEvent)
    setEventInfo(itemInfo)
    // console.log('displayEvent() =>', itemInfo)
  }

  const truncate = (input) => input.length > 40 ? `${input.substring(0, 40)}...` : input;

  const Item = (args) => {
    // console.log('in item args', args)
    let nameParty = truncate(args.event.nameParty)

    return <TouchableOpacity onPress={() => displayEvent(args.event)}>

      <View style={styles.itemContainer}>

        {args.event.urlImageFull !== null ?
          <View style={styles.itemContainerLeft}>
            <Image source={{ uri: args.event.urlImageFull }} style={styles.imageContainer} />
          </View>
          : <View style={styles.itemContainerLeft}>
            <Image source={{ uri: 'https://static.umotive.com/img/product_image_thumbnail_placeholder.png' }} style={styles.imageContainer} />
          </View>
        }

        <View style={styles.itemContainerRight}>


          <Text style={{ color: 'lime', fontSize: 24 }}>{nameParty}</Text>

          <View style={styles.eventDate}>
            <Text style={{ color: 'yellow', fontSize: 18 }}>{moment(args.event.dateStart).format('ll')}</Text>
            <Text style={{ color: 'yellow', fontSize: 18 }}> • </Text>
            <Text style={{ color: 'yellow', fontSize: 18 }}>{args.event.nameType}</Text>
          </View>

          <View style={styles.eventLocation}>
            <Text style={{ color: 'lime', fontSize: 18 }}>{args.event.nameCountry}</Text>
            <Text> </Text>
            <Text style={{ color: 'lime', fontSize: 18 }}>{args.event.nameTown}</Text>
          </View>

        </View>
      </View>
    </TouchableOpacity>
  }


  return (
    <View style={styles.container}>
      {showEvent ?
        <Event showEvent={showEvent} displayEvent={displayEvent} event={eventInfo} index={props.index} resetState={props.resetState} />
        :
        <>
          <FlatList
            data={filtered}
            style={styles.flatlist}
            renderItem={({ item, index }) => <Item event={item} index={index} />}
            keyExtractor={item => item.id}
          />

        </>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#562e7a',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },


  flatlist: {
    width: "100%",
    marginTop: 5,
  },
  itemContainer: {
    flexDirection: 'row',
    width: '95%',
    height: 130,
    marginTop: 5,
    marginLeft: '2.5%',
  },
  imageContainer: {
    width: 120,
    height: 100,
    margin: 10,
  },
  itemContainerLeft: {
    flex: 1,
    margin: 5,
  },
  itemContainerRight: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: -60,
    marginTop: 20,
  },
  eventDate: {
    flexDirection: 'row',
  },
  eventLocation: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});